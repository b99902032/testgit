class Question < ActiveRecord::Base
  attr_accessible :answer, :chapter, :ques_text
  mount_uploader :ques_text, QuesTextUploader
end
