class GamemasterController < ApplicationController
  def index
    @question = Question.new

    respond_to do |format|
      format.html
      format.json { render :json => @msgs }
    end
  end

  def create
    @question = Question.new(params[:question])

    respond_to do |format|
      if @question.save
        format.html
      else
        format.html { render :nothing => true }
      end
      format.json { render :nothing => true }
    end
  end
end
