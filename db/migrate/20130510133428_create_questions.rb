class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :answer
      t.integer :chapter

      t.timestamps
    end
  end
end
